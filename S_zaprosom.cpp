﻿#include <iostream>


void printEvenNumbers(int n)
{
	std::cout << "Even: \n";
	for (int i = 1; i <= 2 * n; i++)
	{
		if (i % 2 == 0)
		{
			std::cout << i << "\n  ";
		}
	}
}

void printOddNumbers (int n)
{
	std::cout << "Odd: \n";
	for (int i = 1; i <= 2 * n; i++)
	{
		if (i % 2 != 0)
		{
			std::cout << i << "\n ";
		}
	}
}

int main()
{
	int n = 5;
	printEvenNumbers(n);
	printOddNumbers(n);
	return 0;
}